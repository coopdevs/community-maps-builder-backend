# -*- coding: utf-8 -*-
from . import cm_slug_id_mixin
from . import cm_metadata
from . import cm_presenter_model
from . import cm_map
from . import cm_map_colorschema
from . import cm_form_model
from . import cm_form_submission
from . import cm_form_submission_metadata
from . import cm_place
from . import cm_place_category
from . import cm_place_presenter_metadata
from . import cm_crowdfunding_notification_request
from . import cm_crowdfunding_notification
from . import cm_partner

# TODO: save metadata on crm_lead based on json schema. computed to display nicely